const fs = require('fs');
const assert = require('assert');
const { setActorId, getActorId } = require('./utils.js');
const assertHTMLEqual = require('assert-equal-html').assertEqual;

const { I } = inject();

Given(/^I create a template named '(.*)'$/, async (templateName) => {
    I.click('//a[@data-tab="actors"]');
    I.click('.actors-sidebar button.create-document');

    I.fillField('//*[@id="document-create"]//input[@name="name"]', templateName);
    I.selectOption('//*[@id="document-create"]//select[@name="type"]', '_template');

    I.click('//*[@id="document-create"]/../..//button[@data-button="ok"]');

    let actorId = setActorId(
        templateName,
        await I.executeScript((templateName) => {
            return game.actors.filter((a) => a.name === templateName)[0].id;
        }, templateName)
    );

    I.waitForElement(`#TemplateSheet-Actor-${actorId}`);
});

When(/^I add a component to the '([a-zA-Z0-9_]+)' component in template '(.*)'$/, (componentKey, templateName) => {
    I.wait(0.1);
    let templateId = getActorId(templateName);
    I.click(`#TemplateSheet-Actor-${templateId} div.${componentKey} a.custom-system-template-tab-controls-add-element`);
});

When(/^I edit the component '(.*)' in template '(.*)'$/, async (componentKey, templateName) => {
    let templateId = getActorId(templateName);

    let baseLocator = `#TemplateSheet-Actor-${templateId} div.${componentKey}`;

    if (await I.grabNumberOfVisibleElements(baseLocator + '.custom-system-editable-component')) {
        // If editable is on the same element as the key
        I.click(baseLocator + '.custom-system-editable-component');
    } else {
        // If editable is on a sub-element of the key (note the space)
        I.click(baseLocator + ' .custom-system-editable-component');
    }

    I.wait(0.5);
});

When(/^I edit the container '(.*)' in template '(.*)'$/, async (componentKey, templateName) => {
    let templateId = getActorId(templateName);

    let baseLocator = `//*[@id="TemplateSheet-Actor-${templateId}"]//div[contains(concat(" ", normalize-space(@class), " "), " ${componentKey} ")]/../`;

    I.click(
        baseLocator + '*[contains(concat(" ", normalize-space(@class), " "), " custom-system-editable-component ")]'
    );

    I.wait(0.5);
});

When(/^I choose '([a-zA-Z- ]+)' as component type$/, (compType) => {
    I.selectOption('#compType', compType);
});

When(/^I type '(.*)' as component key$/, (componentKey) => {
    I.fillField('#compKey', componentKey);
});

When(/^I type '(.*)' as component tooltip$/, (componentTooltip) => {
    I.fillField('#compTooltip', componentTooltip);
});

When(
    /^I move the '(.*)' (component|container) to the last position of container '(.*)' in template '(.*)'$/,
    async (componentKey, draggedType, containerKey, templateName) => {
        let templateId = getActorId(templateName);

        let componentLocator;
        switch (draggedType) {
            case 'component':
                componentLocator = `#TemplateSheet-Actor-${templateId} div.${componentKey}`;

                if (await I.grabNumberOfVisibleElements(componentLocator + '.custom-system-editable-component')) {
                    // If editable is on the same element as the key
                    componentLocator += '.custom-system-editable-component';
                } else {
                    // If editable is on a sub-element of the key (note the space)
                    componentLocator += ' .custom-system-editable-component';
                }
                break;
            case 'container':
                componentLocator = `//*[@id="TemplateSheet-Actor-${templateId}"]//div[contains(concat(" ", normalize-space(@class), " "), " ${componentKey} ")]/../*[contains(concat(" ", normalize-space(@class), " "), " custom-system-editable-component ")]`;
                break;
        }

        let newPositionLocator = `#TemplateSheet-Actor-${templateId} div.${containerKey} a.custom-system-template-tab-controls-add-element`;

        I.dragAndDrop(componentLocator, newPositionLocator);
    }
);

Then(/^An template sheet is opened for Actor '(.*)'$/, async (actorName) => {
    I.seeElement('._template.custom-system-actor');

    let currentName = await I.grabValueFrom('.charname input');
    assert.equal(currentName, actorName);
});

Then(/^the template '(.*)' looks like '(.*)'$/, async (templateName, screenName) => {
    let templateId = getActorId(templateName);

    await I.executeScript((actorId) => {
        let actorDivLocator = `#TemplateSheet-Actor-${actorId}`;
        $(actorDivLocator).css({ position: 'unset' }).off('drag');
    }, templateId);

    I.wait(1);

    I.screenshotElement(`#TemplateSheet-Actor-${templateId} section`, 'template/' + screenName);
    I.seeVisualDiff('template/' + screenName + '.png', {
        tolerance: 0.12,
        prepareBaseImage: false
    });

    await I.executeScript((actorId) => {
        let actorDivLocator = `#TemplateSheet-Actor-${actorId}`;
        $(actorDivLocator).css({ position: '' });
    }, templateId);
});

Then(/^the template '(.*)' HTML is '(.*)'$/, async (templateName, templateHTMLCode) => {
    let templateId = getActorId(templateName);

    let actualHTML = await I.grabHTMLFrom(`#TemplateSheet-Actor-${templateId} section`);
    let expectedHTML = fs.readFileSync('tests/expected_data/templates_html/' + templateHTMLCode + '.html', 'utf8');

    assertHTMLEqual(actualHTML, expectedHTML);
});

Then(/^the template '(.*)' is defined as '(.*)'$/, async (templateName, templateJSONCode) => {
    let templateId = getActorId(templateName);
    let actualJSON = await I.executeScript((actorId) => {
        return game.actors.get(actorId).system;
    }, templateId);

    let expectedJSON = require('./../expected_data/templates_json/' + templateJSONCode + '.json');

    assert.deepStrictEqual(actualJSON, expectedJSON);
});
