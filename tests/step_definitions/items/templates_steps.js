const fs = require('fs');
const assert = require('assert');
const { setActorId, getActorId } = require('../utils.js');
const assertHTMLEqual = require('assert-equal-html').assertEqual;

const { I } = inject();

Given(/^I create an item template named '(.*)'$/, async (templateName) => {
    I.click('//a[@data-tab="items"]');
    I.click('.items-sidebar button.create-document');

    I.fillField('//*[@id="document-create"]//input[@name="name"]', templateName);
    I.selectOption('//*[@id="document-create"]//select[@name="type"]', '_equippableItemTemplate');

    I.click('//*[@id="document-create"]/../..//button[@data-button="ok"]');

    let itemId = setActorId(
        templateName,
        await I.executeScript((templateName) => {
            return game.items.filter((i) => i.name === templateName)[0].id;
        }, templateName)
    );

    I.waitForElement(`#EquippableItemTemplateSheet-Item-${itemId}`);
});

When(/^I add a component to the '([a-zA-Z0-9_]+)' component in item template '(.*)'$/, (componentKey, templateName) => {
    I.wait(0.1);
    let templateId = getActorId(templateName);
    I.click(
        `#EquippableItemTemplateSheet-Item-${templateId} div.${componentKey} a.custom-system-template-tab-controls-add-element`
    );
});

When(/^I edit the component '(.*)' in item template '(.*)'$/, async (componentKey, templateName) => {
    let templateId = getActorId(templateName);

    let baseLocator = `#EquippableItemTemplateSheet-Item-${templateId} div.${componentKey}`;

    if (await I.grabNumberOfVisibleElements(baseLocator + '.custom-system-editable-component')) {
        // If editable is on the same element as the key
        I.click(baseLocator + '.custom-system-editable-component');
    } else {
        // If editable is on a sub-element of the key (note the space)
        I.click(baseLocator + ' .custom-system-editable-component');
    }

    I.wait(0.5);
});

Then(/^An item template sheet is opened for Item '(.*)'$/, async (actorName) => {
    I.seeElement('._equippableItemTemplate.custom-system-actor');

    let currentName = await I.grabValueFrom('.itemname input');
    assert.equal(currentName, actorName);
});

Then(/^the item template '(.*)' looks like '(.*)'$/, async (templateName, screenName) => {
    let templateId = getActorId(templateName);

    await I.executeScript((actorId) => {
        let actorDivLocator = `#EquippableItemTemplateSheet-Item-${actorId}`;
        $(actorDivLocator).css({ position: 'unset' }).off('drag');
    }, templateId);

    I.wait(1);

    I.screenshotElement(`#EquippableItemTemplateSheet-Item-${templateId} section`, 'item/' + screenName);
    I.seeVisualDiff('item/' + screenName + '.png', {
        tolerance: 0.12,
        prepareBaseImage: false
    });

    await I.executeScript((actorId) => {
        let actorDivLocator = `#EquippableItemTemplateSheet-Item-${actorId}`;
        $(actorDivLocator).css({ position: '' });
    }, templateId);
});
