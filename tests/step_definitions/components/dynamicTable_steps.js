const { I } = inject();
const { getActorId } = require('../utils.js');

/**********************************************/
/*            TEMPLATE STEPS                  */
/**********************************************/

When(/^I '(check|uncheck)' make column name bold$/, (action) => {
    switch (action) {
        case 'check':
            I.checkOption('#tableHead');
            break;
        case 'uncheck':
            I.uncheckOption('#tableHead');
            break;
    }
});

When(/^I '(check|uncheck)' show confirmation dialog on row delete$/, (action) => {
    switch (action) {
        case 'check':
            I.checkOption('#tableDeleteWarning');
            break;
        case 'uncheck':
            I.uncheckOption('#tableDeleteWarning');
            break;
    }
});

When(
    /^I edit the column '(.*)' in the dynamic table '(.*)' in template '(.*)'$/,
    (columnKey, dynamicTableKey, templateName) => {
        let templateId = getActorId(templateName);

        I.click(
            `#TemplateSheet-Actor-${templateId} div.${dynamicTableKey} span.custom-system-editable-component.${columnKey}`
        );
    }
);

When(
    /^I move the column '(.*)' in the dynamic table '(.*)' in template '(.*)' to the '(.*)'$/,
    (columnKey, dynamicTableKey, templateName, direction) => {
        let templateId = getActorId(templateName);

        let selector =
            `//*[@id="TemplateSheet-Actor-${templateId}"]` +
            `//div[contains(concat(" ", normalize-space(@class), " "), " ${dynamicTableKey} ")]` +
            `//span[contains(concat(" ", normalize-space(@class), " "), " ${columnKey} ")][contains(concat(" ", normalize-space(@class), " "), " custom-system-editable-component ")]` +
            `/../a[contains(concat(" ", normalize-space(@class), " "), " custom-system-sort-${direction} ")]`;

        I.click(selector);
    }
);

When(/^I type '(.*)' as component column name$/, (value) => {
    I.fillField('#compColName', value);
});

When(/^I select '(.*)' as column alignment$/, (value) => {
    I.selectOption('#compAlign', value);
});

/**********************************************/
/*            CHARACTER STEPS                 */
/**********************************************/

When(/^I add a row to the '(.*)' dynamic table in the '(.*)' character$/, (dynamicTableKey, characterName) => {
    let characterId = getActorId(characterName);

    I.click(`#CharacterSheet-Actor-${characterId} div.${dynamicTableKey} a.custom-system-addDynamicLine`);
});

When(
    /^I type '(.*)' in '(.*)' in the row '(.*)' of the '(.*)' dynamic table in the '(.*)' character$/,
    async (contents, fieldKey, rowNum, dynamicTableKey, characterName) => {
        let characterId = getActorId(characterName);

        // Substracting 1 as dynamic tables are indexed from 0
        rowNum--;

        let inputLocator = `#CharacterSheet-Actor-${characterId} .${dynamicTableKey}\\.${rowNum}\\.${fieldKey} input[type=text]`;

        let previousValue = await I.grabValueFrom(inputLocator);

        I.click(inputLocator);
        for (let i = 0; i < previousValue.length; i++) {
            I.pressKey('Backspace');
        }
        I.type(contents);
        I.wait(0.5);
        I.pressKey('Tab');
    }
);

When(
    /^I click on the '(.*)' checkbox in the row '(.*)' of the '(.*)' dynamic table in the '(.*)' character$/,
    (fieldKey, rowNum, dynamicTableKey, characterName) => {
        let characterId = getActorId(characterName);

        // Substracting 1 as dynamic tables are indexed from 0
        rowNum--;

        I.click(
            `#CharacterSheet-Actor-${characterId} .${dynamicTableKey}\\.${rowNum}\\.${fieldKey} input[type="checkbox"]`
        );
    }
);

When(
    /^I sort '(.*)' the row '(.*)' of the '(.*)' dynamic table in the '(.*)' character$/,
    (direction, rowNum, dynamicTableKey, characterName) => {
        let characterId = getActorId(characterName);
        // Adding 1 for the header row
        rowNum++;

        let locator = `#CharacterSheet-Actor-${characterId} .${dynamicTableKey} tr:nth-child(${rowNum}) a.custom-system-sort${
            direction.charAt(0).toUpperCase() + direction.slice(1)
        }DynamicLine`;

        // Force click to work after screenshots. Dunno why click does not work...
        I.forceClick(locator);
    }
);

When(
    /^I delete the row '(.*)' of the '(.*)' dynamic table in the '(.*)' character$/,
    (rowNum, dynamicTableKey, characterName) => {
        let characterId = getActorId(characterName);
        // Adding 1 for the header row
        rowNum++;

        I.click(
            `#CharacterSheet-Actor-${characterId} .${dynamicTableKey} tr:nth-child(${rowNum}) .custom-system-deleteDynamicLine`
        );

        I.wait(0.1);
    }
);
