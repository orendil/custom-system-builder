const { getActorId } = require('../utils.js');
const { I } = inject();

/**********************************************/
/*            TEMPLATE STEPS                  */
/**********************************************/

When(/^I type '(.*)' as checkbox label$/, (contents) => {
    I.fillField('#checkboxLabel', contents);
});

/**********************************************/
/*            CHARACTER STEPS                 */
/**********************************************/

When(/^I click on the '(.*)' checkbox in the '(.*)' character$/, (fieldKey, characterName) => {
    let characterId = getActorId(characterName);

    I.click(`#CharacterSheet-Actor-${characterId} .${fieldKey} input[type="checkbox"]`);
});

Then(/^The '(.*)' checkbox is '(checked|not checked)' in the '(.*)' character$/, (fieldKey, action, characterName) => {
    let characterId = getActorId(characterName);

    switch (action) {
        case 'checked':
            I.seeCheckboxIsChecked(`#CharacterSheet-Actor-${characterId} .${fieldKey} input[type="checkbox"]`);
            break;
        case 'not checked':
            I.dontSeeCheckboxIsChecked(`#CharacterSheet-Actor-${characterId} .${fieldKey} input[type="checkbox"]`);
            break;
    }
});
