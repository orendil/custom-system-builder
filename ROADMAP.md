# Planned Features

# Labels

- 🚧️ : In progress
- ✔️: Done waiting for release
- 💤️ : Waiting for development

# Scheduled for 2.3.0

- ✔ Undo functionality (Ctrl+Z)
- 🚧 custom-system-builder#126 - Allow roll messages to edit values on the sheet
- 💤️ custom-system-builder#215 - Add delete warning on component deletion
- 💤️ custom-system-builder#166 - Predefined lines in Dynamic Tables
- 💤️ custom-system-builder#170 - User input dialog rework, with more field choices like dropdown or number fields
- 💤️ custom-system-builder#5 - Ability to add items to other items
- 💤️ custom-system-builder#155 - Conditional item & active effects modifiers

# 2.2.0

- ✔️custom-system-builder#82 Added Drag&Drop on components to allow movement everywhere
